<?php

use Drupal\degov_common\DegovModuleUpdater;

/**
 * Implements hook_uninstall().
 */
function degov_media_image_uninstall() {
  // Remove the image bundle dependency on the entity media browser.
  $dependencies = \Drupal::config('entity_browser.browser.media_browser')
    ->get('dependencies.config');
  foreach ($dependencies as $key => $dependency) {
    if ($dependency === 'media.type.image') {
      unset($dependencies[$key]);
    }
  }
  $dependencies = array_values($dependencies);
  \Drupal::configFactory()->getEditable('entity_browser.browser.media_browser')
    ->set('dependencies.config', $dependencies)
    ->save(TRUE);

  // Remove the image tab from the entity media browser.
  $key = 'cf5d9a31-e053-413a-8666-a2c3fedbc4a0';
  $widgets = \Drupal::config('entity_browser.browser.media_browser')
    ->get('widgets');
  if (!empty($widgets[$key])) {
    unset($widgets[$key]);
    \Drupal::configFactory()->getEditable('entity_browser.browser.media_browser')
      ->set('widgets', $widgets)
      ->save(TRUE);
  }
}

/**
 * Migrate image field type from image_immutable to image_image
 */
function degov_media_image_update_8001() {
  \Drupal::configFactory()->getEditable('core.entity_form_display.media.image.media_browser')
    ->set('content.image.type', 'image_image')
    ->save(TRUE);
}

/**
 * Add type svg to image types
 */
function degov_media_image_update_8002() {
  \Drupal::configFactory()->getEditable('core.entity_form_display.media.image.media_browser')
    ->set('settings.file_extensions', 'png gif jpg jpeg svg')
    ->save(TRUE);
}

/**
 * Add missing view mode preview wide
 */
function degov_media_image_update_8003() {
  /**
   * @var DegovModuleUpdater $degovModuleUpdater
   */
  $degovModuleUpdater = \Drupal::service('degov_config.module_updater');
  $degovModuleUpdater->applyUpdates('degov_media_image', '8003');
}
